package pages.mockbin

import geb.Page;
import geb.Browser;
//import groovy.grape.Grape
//@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.5.2')

class MockBinPage extends Page {
    static at = {title == "Mockbin by Mashape"}
    static url =  "https://mockbin.intershop.de/bin/ba041111-130f-47e8-9744-924e615f5444/log"
    static content = {
        /*Elements specific to Mockbin */
        jsonRequests  {$("span.hljs-string")*.getAttribute("innerText").findAll{key -> key.contains('key=03')}}
        order {orderId,state -> jsonRequests.findAll{key-> key.contains(orderId) and key.contains(state)} }

        //        inner {$("span.hljs-string")*.getAttribute("innerText")}
        //        order {orderId -> inner.findAll{key -> key.contains('reference=$orderId')}}
    }
    /**
     * Method to post mockbin notifications
     */
    def postNotification() {
        jsonRequests.eachWithIndex{it,index->
            def postURL = "http://qa3.intershop.de:81/INTERSHOP/web/BOS/inSPIRED-inTRONICS-Site/en_US/-/EUR/ViewPayone-Notify?"+it
            ((HttpURLConnection)new URL(postURL).openConnection()).with({
                requestMethod = 'POST'
                println inputStream.text + " - $index"// Using 'inputStream.text' because 'content' will throw an exception when empty.
            })
            true
        }
        println "------"
    }

    /**
     * Method to wait for N seconds
     */
    def sleepForNSeconds(int n)
    {

        def originalMilliseconds = System.currentTimeMillis()
        refreshWaitFor {}
        waitFor(n + 1)
        {
            (System.currentTimeMillis() - originalMilliseconds) > (n * 2000)
        }
    }
}
