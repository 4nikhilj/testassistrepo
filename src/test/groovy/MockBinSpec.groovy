import geb.Browser
import geb.driver.CachingDriverFactory
import geb.driver.*
import geb.spock.GebReportingSpec

import pages.mockbin.MockBinPage

class MockBinSpec extends GebReportingSpec  {

    def cleanup() {
        CachingDriverFactory.clearCache()
    }

    def "Post Notification on MockBin Page"()
    {
        when:
        to MockBinPage

        then:
        at MockBinPage

        when:
        def originalMilliseconds = System.currentTimeMillis()
        def Time_13Hours = 50000 * 1000
        while((System.currentTimeMillis() - originalMilliseconds) < Time_13Hours)
        {
            postNotification()
            driver.navigate().refresh()
        }

        then:
        true
    }
}
